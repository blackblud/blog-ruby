Rails.application.routes.draw do

  root "posts#index", as: "home"

  get "main" => "pages#main"
  get "main" => "pages#sub"

  resources :subscribers

resources :posts do
  resources :comments
end

end
